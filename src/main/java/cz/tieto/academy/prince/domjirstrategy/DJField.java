/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy;

import cz.tieto.academy.prince.domjirstrategy.entity.*;
import cz.tieto.academy.prince.domjirstrategy.scenario.*;
import cz.tieto.princegame.common.gameobject.Equipment;
import cz.tieto.princegame.common.gameobject.Field;
import cz.tieto.princegame.common.gameobject.Obstacle;

/**
 *
 * @author Reemon
 */
public class DJField {
    private Field field = null;
    private Equipment equipment = null;
    private Scenario scenario = null;
    
    private static final String PITFALL = "pitfall";
    private static final String CHOPPER = "chopper";
    private static final String KNIGHT = "knight";

    public DJField(Field field) {
        this.field = field;
        
        if(this.field != null) {
            this.equipment = this.field.getEquipment();
            this.scenario = this.pickScenario(this.field.getObstacle());
        }
    }
    
    private Scenario pickScenario(Obstacle obstacle) {
        if(obstacle == null) {
            return new BasicMovementScenario();
        }
        
        if(PITFALL.equals(obstacle.getName())) {
            return new PitfallScenario(new Pitfall(obstacle));
        }
        
        if(CHOPPER.equals(obstacle.getName())) {
            return new ChopperScenario(new Chopper(obstacle));
        }
        
        if(KNIGHT.equals(obstacle.getName())) {
            return new KnightScenario(new Knight(obstacle));
        } 
        
        return null;
    }
    
    public boolean isEnd() {
        return this.field == null;
    }
    
    public boolean isGate() {
        return (!this.isEnd() && this.field.isGate());
    }
    
    public boolean isEquipment() {
        return this.equipment != null;
    }
    
    public Scenario getScenario() {
        return this.scenario;
    }
}
