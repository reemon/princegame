/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.entity;

import cz.tieto.princegame.common.gameobject.Obstacle;

/**
 *
 * @author Reemon
 */
public class Chopper implements Entity {
    private Obstacle obstacle = null;
    private String name = null;
    
    private boolean opening = false;
    private boolean closing = true;
    
    private static final String OPENING = "opening";
    private static final String CLOSING = "closing";
    private static final String TRUE = "true";

    public Chopper(Obstacle obstacle) {
        this.obstacle = obstacle;
        
        if(this.obstacle != null) {
            this.opening = (TRUE.equals(this.obstacle.getProperty(OPENING))) ? true : false;
            this.closing = (TRUE.equals(this.obstacle.getProperty(CLOSING))) ? true : false;
            this.name = this.obstacle.getName();
        }
    }
    
    public boolean isOpening() {
        return (this.opening && !this.closing);
    }
    
    public boolean isClosing() {
        return (!this.opening && this.closing);
    }
    
    public String getName() {
        return this.name;
    }

    public Obstacle getObstacle() {
        return this.obstacle;
    }     
}
