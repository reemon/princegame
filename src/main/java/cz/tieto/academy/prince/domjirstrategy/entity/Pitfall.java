/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.entity;

import cz.tieto.princegame.common.gameobject.Obstacle;

/**
 *
 * @author Reemon
 */
public class Pitfall implements Entity {
    private Obstacle obstacle = null;
    private String name = null;
    
    public Pitfall(Obstacle obstacle) {
        this.obstacle = obstacle;
        
        if(this.obstacle != null) {
            this.name = this.obstacle.getName();
        }
    }

    public String getName() {
        return this.name;
    }

    public Obstacle getObstacle() {
        return this.obstacle;
    }   
}
