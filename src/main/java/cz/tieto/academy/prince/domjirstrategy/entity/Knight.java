/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.entity;

import cz.tieto.princegame.common.gameobject.Obstacle;

/**
 *
 * @author Reemon
 */
public class Knight implements Entity {   
    private Obstacle obstacle = null;
    private int health = 0;
    private boolean dead = true;
    private String name = null;
    
    private static final String HEALTH = "health";
    private static final String DEAD = "dead";
    private static final String TRUE = "true";

    public Knight(Obstacle obstacle) {
        this.obstacle = obstacle;
        if(this.obstacle != null) {
            this.health = Integer.parseInt(this.obstacle.getProperty(HEALTH));
            this.dead = (TRUE.equals(this.obstacle.getProperty(DEAD))) ? true : false;
            this.name = this.obstacle.getName();
        }
    }
    
    public int getHealth() {
        return this.health;
    }
    
    public boolean isDead() {
        return this.dead;
    }
    
    public String getName() {
        return this.name;
    }

    public Obstacle getObstacle() {
        return this.obstacle;
    }   
}
