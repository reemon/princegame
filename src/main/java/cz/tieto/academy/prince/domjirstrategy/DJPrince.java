/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy;

import cz.tieto.academy.prince.domjirstrategy.entity.Entity;
import cz.tieto.academy.prince.domjirstrategy.scenario.HealScenario;
import cz.tieto.academy.prince.domjirstrategy.scenario.Scenario;
import cz.tieto.princegame.common.action.Action;
import cz.tieto.princegame.common.action.EnterGate;
import cz.tieto.princegame.common.action.Grab;
import cz.tieto.princegame.common.action.Heal;
import cz.tieto.princegame.common.action.JumpBackward;
import cz.tieto.princegame.common.action.JumpForward;
import cz.tieto.princegame.common.action.MoveBackward;
import cz.tieto.princegame.common.action.MoveForward;
import cz.tieto.princegame.common.action.Use;
import cz.tieto.princegame.common.gameobject.Equipment;
import cz.tieto.princegame.common.gameobject.Prince;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Reemon
 */
public class DJPrince {
    private Prince prince = null;
    
    private int health = 0;
    private int maxHealth = 0;
    private boolean dirLeft = false;
    private boolean inBattle = false;
    private Collection<Equipment> inventory = null;
    private List<Scenario> requiredActions = new ArrayList<Scenario>();
    
    private static final String SWORD = "sword";
    
    public DJPrince() {
        //this.addAction(null, new HealScenario(null, 0, false)); //nefunguje, i kdyz se prida akce se stejnym id vzdy se jako prvni vola tahle heal metoda
        this.dirLeft = (new Random(System.currentTimeMillis()).nextBoolean());
    }
    
    private void init() {
        if(this.prince != null) {
            this.health = this.prince.getHealth();
            this.maxHealth = this.prince.getMaxHealth();
            this.inventory = this.prince.getInventory();
        }
    }
    
    /*Required actions methods*/
    public Scenario addAction(Integer id, Scenario scenario) {
        if(scenario == null) {
            return null;
        }
        
        if(id == null) {
            this.requiredActions.add(scenario);
        } else {
            this.requiredActions.add(id, scenario);
        }
         
        return scenario;
    }
    
    public boolean removeAction(Integer id) {
        try {
            this.requiredActions.remove(id.intValue());
            return true;
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }
    
    public Action runAction() {
        Action action;
        for(Scenario s : this.requiredActions) {
            action = s.run(this, this.lookAhead());
            if(action != null) {
                return action;
            }
        } 
        
        return null;
    }
    
    /*Regular actions*/
    public Action heal() {
        return new Heal();
    }
    
    public Action grab() {
        return new Grab();
    }
    
    public Action enterGate() {
        return new EnterGate();
    }
    
    public Action useSword(Entity entity) {
        Equipment sword = this.findEquipment(SWORD);
        
        if(entity == null || sword == null) {
            return null;
        }
        
        return new Use(sword, entity.getObstacle());
    }
    
    private Equipment findEquipment(String name) {
        if(name == null) {
            return null;
        }
        
        for(Equipment e : this.inventory) {
            if(e != null && name.equals(e.getName())) {
                return e;
            }
        }
        
        return null;
    }
    
    /*Observe surroundig actions*/
    public DJField lookBehind() {
        int index = (this.dirLeft) ? 1 : -1;
        return new DJField(this.prince.look(index));
    }
    
    public DJField lookAhead() {
        int index = (this.dirLeft) ? -1 : 1;
        return new DJField(this.prince.look(index));
    }
    
    public DJField lookBeneath() {
        return new DJField(this.prince.look(0));
    }
    
    /*Movemement actions*/
    /*Walk, Jump in direction*/
    public Action walk() {
        Action walk = (this.dirLeft) ? new MoveBackward() : new MoveForward();
        return walk;
    }
    
    public Action jump() {
        Action jump = (this.dirLeft) ? new JumpBackward() : new JumpForward();
        return jump;
    }
    
    /*StepBack, JumpBack in opposite direction*/
    /*Prince moves in opposite direction without need to call turn, used in figths with knights for healing time*/
    public Action stepBack() {
        Action stepback = (this.dirLeft) ? new MoveForward() : new MoveBackward();
        return stepback;
    }
    
    public Action jumpBack() {
        Action jumpback = (this.dirLeft) ? new JumpForward() : new JumpBackward();
        return jumpback;
    }
    
    //Otoci hrace a vrati field co je pred nim
    public DJField turn() {
        this.dirLeft = !this.dirLeft;
        return this.lookAhead();
    }
    
    /*Get/Set*/
    public boolean isLookingLeft() {
        return this.dirLeft;
    }
    
    public boolean isHealed() {
        return this.health == this.maxHealth;
    }
    
    public boolean isInBattle() {
        return this.inBattle;
    }
    
    public boolean shouldHeal() {
        return (this.health < (this.maxHealth / 3));
    }
    
    public boolean hasSword() {
        return this.findEquipment(SWORD) != null;
    }
    
    public void setInBattle(boolean value) {
        this.inBattle = value;
    }
    
    public void setPrince(Prince prince) {
        this.prince = prince;
        this.init();
    }
}
