/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.scenario;

import cz.tieto.academy.prince.domjirstrategy.DJField;
import cz.tieto.academy.prince.domjirstrategy.DJPrince;
import cz.tieto.academy.prince.domjirstrategy.entity.Pitfall;
import cz.tieto.princegame.common.action.Action;

/**
 *
 * @author Reemon
 */
public class PitfallScenario implements Scenario {
    private Pitfall pitfall = null;
    
    public PitfallScenario(Pitfall pitfall) {
        this.pitfall = pitfall;
    }
    
    public Action run(DJPrince prince, DJField field) {
        if(this.pitfall == null) {
            return null;
        }
        
        return prince.jump();
    }
    
}
