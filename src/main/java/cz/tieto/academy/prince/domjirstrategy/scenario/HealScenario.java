/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.scenario;

import cz.tieto.academy.prince.domjirstrategy.DJField;
import cz.tieto.academy.prince.domjirstrategy.DJPrince;
import cz.tieto.princegame.common.action.Action;

/**
 *
 * @author Reemon
 */
public class HealScenario implements Scenario {
    private Integer id = null;
    private int minimalDistance = 0;
    private boolean healToMax = false;
    
    public HealScenario(Integer id, int minimalDistance, boolean healToMax) {
        this.id = id;
        this.minimalDistance = minimalDistance;
        this.healToMax = healToMax;
    }

    public Action run(DJPrince prince, DJField field) {
        //Pokud nejsem dost daleko od nebezpeci
        if(this.minimalDistance > 0) {
            Scenario scenario = field.getScenario();
            if(scenario != null) {
                this.minimalDistance -= 1;
                return scenario.run(prince, field);
            }
        }
        
        //Pokud se musim lecit
        if((this.healToMax && !prince.isHealed()) || prince.shouldHeal()) {
            return prince.heal();
        }        
        
        //Pokud se jedna o docasnou akci, tak ji vymaz
        if(this.id != null) {           
            prince.removeAction(id);          
        }
        
        if(prince.isInBattle()) {
            DJField fahead = prince.turn();
            return fahead.getScenario().run(prince, fahead);
        }
        
        //Pokud se jedna o defaultni akci, nedelej nic
        return null;
    }
}
