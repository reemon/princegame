/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.scenario;

import cz.tieto.academy.prince.domjirstrategy.DJField;
import cz.tieto.academy.prince.domjirstrategy.DJPrince;
import cz.tieto.academy.prince.domjirstrategy.entity.Knight;
import cz.tieto.princegame.common.action.Action;

/**
 *
 * @author Reemon
 */
public class KnightScenario implements Scenario {
    private Knight knight = null;
    
    public KnightScenario(Knight knight) {
        this.knight = knight;
    }
    
    public Action run(DJPrince prince, DJField field) {
        if(this.knight == null) {
            return null;
        }
        
        if(this.knight.isDead()) {
            prince.setInBattle(false);
            return prince.walk();
        }
        
        //Nemam mec, otocim se a jdu
        if(!prince.hasSword()) {         
            DJField fahead = prince.turn();    
            return fahead.getScenario().run(prince, fahead); //zavola se scenar pro field ahead
        }
        
        //Mel bych se uzdravit
        if(prince.shouldHeal()) {
            //Pridej docasnou akci pro vyleceni
            Scenario knightHeal = prince.addAction(0, new HealScenario(0, 1, true)); //Nastav jako nejvetsi prioritu => index 0
            
            DJField fahead = prince.turn(); 
            if(knightHeal == null) {
                return fahead.getScenario().run(prince, fahead); //zavola se scenar pro field ahead
            }
            return knightHeal.run(prince, fahead);
        }
        
        prince.setInBattle(true);
        return prince.useSword(knight);
    }  
}
