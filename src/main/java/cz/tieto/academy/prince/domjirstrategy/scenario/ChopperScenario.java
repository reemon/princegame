/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.scenario;

import cz.tieto.academy.prince.domjirstrategy.DJField;
import cz.tieto.academy.prince.domjirstrategy.DJPrince;
import cz.tieto.academy.prince.domjirstrategy.entity.Chopper;
import cz.tieto.princegame.common.action.Action;

/**
 *
 * @author Reemon
 */
public class ChopperScenario implements Scenario {
    private Chopper chopper = null;
    
    public ChopperScenario(Chopper chopper) {
        this.chopper = chopper;
    }
    
    public Action run(DJPrince prince, DJField field) {
        if(this.chopper == null) {
            return null;
        }
        
        if(this.chopper.isOpening()) {
            return prince.jump();
        }
        
        return prince.heal();
    }
    
}
