/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy;

import cz.tieto.princegame.common.GameStrategy;
import cz.tieto.princegame.common.action.Action;
import cz.tieto.princegame.common.gameobject.Prince;

/**
 *
 * @author Reemon
 */
public class DomJirStrategy implements GameStrategy  {
    private DJPrince djprince = new DJPrince();

    @Override
    public Action step(Prince prince) {
        djprince.setPrince(prince);
        
        DJField fbeneath = djprince.lookBeneath();
        if(fbeneath.isGate()) {
            return djprince.enterGate();
        }
        
        if(fbeneath.isEquipment()) {
            return djprince.grab();
        }
        
        Action action = djprince.runAction();
        if(action != null) {
            return action;
        }
        
        DJField fahead = djprince.lookAhead();
        if(fahead.isEnd()) {         
            fahead = djprince.turn();
            return fahead.getScenario().run(djprince, fahead);
        }
        
        return fahead.getScenario().run(djprince, fahead);
    }
    
}
