/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.scenario;

import cz.tieto.academy.prince.domjirstrategy.DJField;
import cz.tieto.academy.prince.domjirstrategy.DJPrince;
import cz.tieto.princegame.common.action.Action;

/**
 *
 * @author Reemon
 */
public interface Scenario{ 
    public Action run(DJPrince prince, DJField field);
}
