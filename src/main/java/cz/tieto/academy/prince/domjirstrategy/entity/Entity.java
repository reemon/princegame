/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tieto.academy.prince.domjirstrategy.entity;

import cz.tieto.princegame.common.gameobject.Obstacle;

/**
 *
 * @author Reemon
 */
public interface Entity {
    public String getName();
    public Obstacle getObstacle();
}
